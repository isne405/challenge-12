#include<string>
#include<list>
#include<iostream>


using namespace std;

class student{
	public:
		student(int newSID , string newSName){
			SID = newSID;
			SName = newSName;
		}

		int getID(){return SID;}

		string getName(){return SName;}

	private:
		int SID;
		string SName;
};

int Hash_ID(int);
void Table_set(student , list<student> [50]);
int strHash(string , const int);
void init_tableset(student , list<student> [50]);


void main(){
	int tmpID;
	string tmpName;
	list<student> ISNE[50];
	student GG(12345 , "John");
	

	cout << "Enter Student's ID : ";
	cin >> tmpID;

	cout << "Enter Student's Name : ";
	cin >> tmpName;
	
	student tmpStudent(tmpID , tmpName);
	init_tableset(tmpStudent , ISNE);

	
	//Table_set(GG , ISNE);

	student s2 = ISNE[5].front();
	cout << s2.getName();
}


int Hash_ID(int Key){
	return Key%50; 
}

void Table_set(student s1 , list<student> ISNE[50] ){
	int method;
	cout << "How do you Hash 1 as Hash by ID 2 as Hash by name : ";
	cin >> method;
	if(method == 1){
		int index = Hash_ID(s1.getID());
		ISNE[index].push_back(s1);
	}else if(method == 2){
		int index = strHash(s1.getName() , 50);
		ISNE[index].push_back(s1);
	}
}

int strHash(string toHash, const int TableSize) {
	int hashValue = 0;
	for (unsigned int Pos = 0; Pos < toHash.length(); Pos++) {
		hashValue = hashValue + int(toHash.at(Pos));
	}
	return (hashValue % TableSize);
}

void init_tableset(student s1 , list<student> ISNE[50]){
	Table_set(s1 , ISNE);
}